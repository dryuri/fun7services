package com.fun7.services.unit.services;

import com.fun7.services.services.ServicesService;
import com.fun7.services.services.system.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = "spring.main.allow-bean-definition-overriding=true")
public class ServicesServiceTest {

    @MockBean
    private AdsService adsService;

    @MockBean
    private CustomerSupportService customerSupportService;

    @MockBean
    private MultiplayerService multiplayerService;

    @Autowired
    private ServicesService servicesService;

    @Test
    public void testGetSystemServices() {
        Set<String> result = this.servicesService.getSystemServices();
        assertTrue(result.contains("multiplayer"));
        assertTrue(result.contains("customer-support"));
        assertTrue(result.contains("ads"));
    }

    @Test
    public void isServiceEnabledMultiplayerEnabled() {
        Mockito.when(this.multiplayerService.isEnabled(any())).thenReturn(true);
        assertTrue(this.servicesService.isServiceEnabled("multiplayer", null));
    }

    @Test
    public void isServiceEnabledMultiplayerDisabled() {
        Mockito.when(this.multiplayerService.isEnabled(any())).thenReturn(false);
        assertFalse(this.servicesService.isServiceEnabled("multiplayer", null));
    }

    @Test
    public void isServiceEnabledCustomerSupportEnabled() {
        Mockito.when(this.customerSupportService.isEnabled(any())).thenReturn(true);
        assertTrue(this.servicesService.isServiceEnabled("customer-support", null));
    }

    @Test
    public void isServiceEnabledCustomerSupportDisabled() {
        Mockito.when(this.customerSupportService.isEnabled(any())).thenReturn(false);
        assertFalse(this.servicesService.isServiceEnabled("customer-support", null));
    }

    @Test
    public void isServiceEnabledAdsEnabled() {
        Mockito.when(this.adsService.isEnabled(any())).thenReturn(true);
        assertTrue(this.servicesService.isServiceEnabled("ads", null));
    }

    @Test
    public void isServiceEnabledAdsDisabled() {
        Mockito.when(this.adsService.isEnabled(any())).thenReturn(false);
        assertFalse(this.servicesService.isServiceEnabled("ads", null));
    }

    @Test
    public void isServiceEnabledUnknownDisabled() {
        assertFalse(this.servicesService.isServiceEnabled("unknown", null));
    }
}
