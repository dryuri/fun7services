package com.fun7.services.unit.services;

import com.fun7.services.controllers.UserData;
import com.fun7.services.services.ads.ExternalAdResponse;
import com.fun7.services.services.ads.ExternalAdService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = "spring.main.allow-bean-definition-overriding=true")
public class ExternalAdServiceTest {

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private ExternalAdService externalAdService;

    @Test
    public void testExternalAdServiceSuccess() {
        ExternalAdResponse externalAdResponse = new ExternalAdResponse("this is an ad");

        ResponseEntity<ExternalAdResponse> response = new ResponseEntity<>(externalAdResponse, null, HttpStatus.OK);

        when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.any(HttpMethod.class),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<ExternalAdResponse>>any()))
                .thenReturn(response);

        assertEquals("this is an ad", this.externalAdService.getAds(new UserData("user7", "US", "Europe/Ljubljana", "my-agent")).getAds());
    }

    @Test
    public void testExternalAdServiceException() {
        when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.any(HttpMethod.class),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<ExternalAdResponse>>any()))
                .thenReturn(null);

        Assertions.assertThrows(Exception.class, () -> {
            this.externalAdService.getAds(new UserData("myUser", "US", "Europe/Ljubljana", "myUserAgent"));
        });
    }
}
