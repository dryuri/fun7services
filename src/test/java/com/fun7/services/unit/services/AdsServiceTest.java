package com.fun7.services.unit.services;

import com.fun7.services.controllers.UserData;
import com.fun7.services.services.ads.ExternalAdResponse;
import com.fun7.services.services.ads.ExternalAdService;
import com.fun7.services.services.system.AdsService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = "spring.main.allow-bean-definition-overriding=true")
public class AdsServiceTest {

    @MockBean
    private ExternalAdService externalAdService;

    @Autowired
    private AdsService adsService;

    @Test
    public void testServiceEnabled() {
        Mockito.when(this.externalAdService.getAds(any())).thenReturn(new ExternalAdResponse("sure, why not!"));
        assertTrue(adsService.isEnabled(new UserData("user7", "US", "Europe/Ljubljana", "")));
    }

    @Test
    public void testServiceDisabled() {
        Mockito.when(this.externalAdService.getAds(any())).thenReturn(new ExternalAdResponse("you shall not pass"));
        assertFalse(adsService.isEnabled(new UserData("user7", "US", "Europe/Ljubljana", "")));
    }
}
