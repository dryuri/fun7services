package com.fun7.services.unit.services;

import com.fun7.services.entities.Gameplay;
import com.fun7.services.repositories.GameRepository;
import com.fun7.services.services.game.GameService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = "spring.main.allow-bean-definition-overriding=true")
public class GameServiceTest {

    @MockBean
    private GameRepository gameRepository;

    @Autowired
    private GameService gameService;

    @Test
    public void testPlaygame() {
        Mockito.when(gameRepository.save(any())).thenReturn(new Gameplay("user7", null));
        this.gameService.playGame("user7");
        verify(gameRepository, times(1)).save(any());
    }

    @Test
    public void testIsCountryEnabledEnabledUpper() {
        assertTrue(this.gameService.isCountryAllowed("US"));
    }

    @Test
    public void testIsCountryEnabledEnabledLower() {
        assertTrue(this.gameService.isCountryAllowed("us"));
    }

    @Test
    public void testIsCountryEnabledDisabled() {
        assertFalse(this.gameService.isCountryAllowed("SL"));
    }
}
