package com.fun7.services.unit.services;

import com.fun7.services.controllers.UserData;
import com.fun7.services.repositories.GameRepository;
import com.fun7.services.services.game.GameService;
import com.fun7.services.services.system.MultiplayerService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = "spring.main.allow-bean-definition-overriding=true")
public class MultiplayerServiceTest {

    @MockBean
    private GameService gameService;

    @MockBean
    private GameRepository gameRepository;

    @Autowired
    private MultiplayerService multiplayerService;

    @Test
    public void testMultiplayerDisabledNotEnoughPlaysAndNotFromUS() {
        Mockito.when(this.gameRepository.countByUserId(any())).thenReturn(2);
        Mockito.when(this.gameService.isCountryAllowed(any())).thenReturn(false);

        assertFalse(this.multiplayerService.isEnabled(new UserData("user7", "SL", "Europe/Ljubljana", "")));
    }

    @Test
    public void testMultiplayerDisabledNotEnoughPlaysAndFromUS() {
        Mockito.when(this.gameRepository.countByUserId(any())).thenReturn(2);
        Mockito.when(this.gameService.isCountryAllowed(any())).thenReturn(true);

        assertFalse(this.multiplayerService.isEnabled(new UserData("user7", "SL", "Europe/Ljubljana", "")));
    }

    @Test
    public void testMultiplayerDisabledEnoughPlaysAndNotFromUS() {
        Mockito.when(this.gameRepository.countByUserId(any())).thenReturn(6);
        Mockito.when(this.gameService.isCountryAllowed(any())).thenReturn(false);

        assertFalse(this.multiplayerService.isEnabled(new UserData("user7", "SL", "Europe/Ljubljana", "")));
    }

    @Test
    public void testMultiplayerEnabled() {
        Mockito.when(this.gameRepository.countByUserId(any())).thenReturn(6);
        Mockito.when(this.gameService.isCountryAllowed(any())).thenReturn(true);

        assertTrue(this.multiplayerService.isEnabled(new UserData("user7", "SL", "Europe/Ljubljana", "")));
    }
}
