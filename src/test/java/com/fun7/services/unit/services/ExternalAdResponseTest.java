package com.fun7.services.unit.services;

import com.fun7.services.services.ads.ExternalAdResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
public class ExternalAdResponseTest {

    @Test
    public void testExternalAdResponse() {
        ExternalAdResponse externalAdResponse = new ExternalAdResponse("ads here");

        assertEquals("ads here", externalAdResponse.getAds());

        externalAdResponse.setAds("something else");

        assertEquals("something else", externalAdResponse.getAds());
    }
}
