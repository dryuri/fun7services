package com.fun7.services.unit.services;

import com.fun7.services.services.system.CustomerSupportService;
import com.fun7.services.utils.HolidayService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.ZonedDateTime;
import java.util.TimeZone;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = "spring.main.allow-bean-definition-overriding=true")
public class CustomerSupportServiceTest {

    @MockBean
    private HolidayService holidayService;


    @Autowired
    private CustomerSupportService customerSupportService;

    @Test
    public void testNotAWorkDay() {
        Mockito.when(this.holidayService.isWorkDay(any())).thenReturn(false);
        Mockito.when(this.holidayService.getLjubljanaTimeNow()).thenReturn(null);
        assertFalse(this.customerSupportService.isEnabled(null));
    }

    @Test void testWorkDayWorkingHours() {
        TimeZone timeZoneLjubljana = TimeZone.getTimeZone("Europe/Ljubljana");

        Mockito.when(this.holidayService.isWorkDay(any())).thenReturn(true);
        Mockito.when(this.holidayService.getLjubljanaTimeNow()).thenReturn(ZonedDateTime.of(2019,12,17,10,0,0,0,timeZoneLjubljana.toZoneId()));

        assertTrue(this.customerSupportService.isEnabled(null));
    }

    @Test void testWorkDayTooEarly() {
        TimeZone timeZoneLjubljana = TimeZone.getTimeZone("Europe/Ljubljana");

        Mockito.when(this.holidayService.isWorkDay(any())).thenReturn(true);
        Mockito.when(this.holidayService.getLjubljanaTimeNow()).thenReturn(ZonedDateTime.of(2019,12,17,8,0,0,0,timeZoneLjubljana.toZoneId()));

        assertFalse(this.customerSupportService.isEnabled(null));
    }

    @Test void testWorkDayTooLate() {
        TimeZone timeZoneLjubljana = TimeZone.getTimeZone("Europe/Ljubljana");

        Mockito.when(this.holidayService.isWorkDay(any())).thenReturn(true);
        Mockito.when(this.holidayService.getLjubljanaTimeNow()).thenReturn(ZonedDateTime.of(2019,12,17,16,0,0,0,timeZoneLjubljana.toZoneId()));

        assertFalse(this.customerSupportService.isEnabled(null));
    }
}
