package com.fun7.services.unit.utils;

import com.fun7.services.utils.Holiday;
import com.fun7.services.utils.HolidayService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.TimeZone;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = "spring.main.allow-bean-definition-overriding=true")
public class HolidayServiceTest {

    @Autowired
    private HolidayService holidayService;

    @Test
    public void testHolidays() {
        List<Holiday> holidays = this.holidayService.getHolidays();

        assertEquals(2, holidays.size());
        assertEquals(25,holidays.get(0).getDay());
        assertEquals(12,holidays.get(0).getMonth());

        assertEquals(26,holidays.get(1).getDay());
        assertEquals(12,holidays.get(1).getMonth());
    }

    @Test
    public void testIsWorkDayWeekend() {
        TimeZone timeZoneLjubljana = TimeZone.getTimeZone("Europe/Ljubljana");

        //Saturday
        ZonedDateTime weekendDay = ZonedDateTime.of(2019,12,21,16,0,0,0,timeZoneLjubljana.toZoneId());
        assertFalse(this.holidayService.isWorkDay(weekendDay));

        //Sunday
        weekendDay = ZonedDateTime.of(2019,12,22,16,0,0,0,timeZoneLjubljana.toZoneId());
        assertFalse(this.holidayService.isWorkDay(weekendDay));
    }

    @Test
    public void testIsWorkDayWeekDay() {
        TimeZone timeZoneLjubljana = TimeZone.getTimeZone("Europe/Ljubljana");

        //Saturday
        ZonedDateTime weekendDay = ZonedDateTime.of(2019,12,20,16,0,0,0,timeZoneLjubljana.toZoneId());
        assertTrue(this.holidayService.isWorkDay(weekendDay));
    }

    @Test
    public void testIsWorkDayHolidayDuringWeek() {
        TimeZone timeZoneLjubljana = TimeZone.getTimeZone("Europe/Ljubljana");

        ZonedDateTime holiday = ZonedDateTime.of(2019,12,25,16,0,0,0,timeZoneLjubljana.toZoneId());
        assertFalse(this.holidayService.isWorkDay(holiday));

        holiday = ZonedDateTime.of(2019,12,26,16,0,0,0,timeZoneLjubljana.toZoneId());
        assertFalse(this.holidayService.isWorkDay(holiday));
    }

    @Test
    public void testGetLjubljanaTimeNow() {
        TimeZone timeZoneLjubljana = TimeZone.getTimeZone("Europe/Ljubljana");
        ZonedDateTime ljubljanaNowExpected = ZonedDateTime.now(timeZoneLjubljana.toZoneId());

        ZonedDateTime ljubljanaNowActual = this.holidayService.getLjubljanaTimeNow();

        Duration duration = Duration.between(ljubljanaNowActual, ljubljanaNowExpected);
        long diff = Math.abs(duration.toMinutes());

        assertTrue(diff <= 1);
    }
}
