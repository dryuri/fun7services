package com.fun7.services.unit.utils;

import com.fun7.services.utils.Holiday;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
public class HolidayTest {

    @Test
    public void testHoliday() {
        Holiday holiday = new Holiday(1,2);

        assertEquals(1,holiday.getDay());
        assertEquals(2,holiday.getMonth());

        holiday.setDay(3);
        holiday.setMonth(4);

        assertEquals(3,holiday.getDay());
        assertEquals(4,holiday.getMonth());
    }
}
