package com.fun7.services.unit.controllers;

import com.fun7.services.controllers.UserData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
public class UserDataTest {

    @Test
    public void testConstructorGettersSetters() {
        UserData userData = new UserData("user7", "US", "Europe/Ljubljana", "");

        assertEquals("user7",userData.getUserId());
        assertEquals("US",userData.getCc());
        assertEquals("Europe/Ljubljana",userData.getTimezone());

        userData.setCc("SL");
        userData.setUserId("newuser");
        userData.setTimezone("Europe/Frankfurt");

        assertEquals("newuser",userData.getUserId());
        assertEquals("SL",userData.getCc());
        assertEquals("Europe/Frankfurt",userData.getTimezone());
    }
}
