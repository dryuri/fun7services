package com.fun7.services.unit.controllers;

import com.fun7.services.controllers.ServicesController;
import com.fun7.services.entities.Gameplay;
import com.fun7.services.services.ServicesService;
import com.fun7.services.services.game.GameService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;

@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = "spring.main.allow-bean-definition-overriding=true")
public class ServicesControllerUnitTest {

    @MockBean
    private ServicesService servicesService;

    @MockBean
    private GameService gameService;

    @Autowired
    private ServicesController servicesController;

    @Test
    public void testServicesEnabled() {
        Set<String> enabledServices = new TreeSet<>();
        enabledServices.add("multiplayer");
        enabledServices.add("customer-support");
        enabledServices.add("ads");

        doNothing().when(this.gameService).playGame(any());
        Mockito.when(this.servicesService.getSystemServices()).thenReturn(enabledServices);
        Mockito.when(this.servicesService.isServiceEnabled(any(), any())).thenReturn(true);

        Map<String, String> response = servicesController.services(null, "user7", "US", "Europe/Ljubljana");

        assertEquals("enabled", response.get("multiplayer"));
        assertEquals("enabled", response.get("customer-support"));
        assertEquals("enabled", response.get("ads"));
    }

    @Test
    public void testServicesDisabled() {
        Set<String> enabledServices = new TreeSet<>();
        enabledServices.add("multiplayer");
        enabledServices.add("customer-support");
        enabledServices.add("ads");

        doNothing().when(this.gameService).playGame(any());
        Mockito.when(this.servicesService.getSystemServices()).thenReturn(enabledServices);
        Mockito.when(this.servicesService.isServiceEnabled(any(), any())).thenReturn(false);

        Map<String, String> response = servicesController.services(null, "user7", "US", "Europe/Ljubljana");

        assertEquals("disabled", response.get("multiplayer"));
        assertEquals("disabled", response.get("customer-support"));
        assertEquals("disabled", response.get("ads"));
    }
}
