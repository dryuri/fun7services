package com.fun7.services.unit.controllers;

import com.fun7.services.exceptions.ErrorResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
public class ErrorResponseTest {

    @Test
    public void testErrorResponse() {
        ErrorResponse errorResponse = new ErrorResponse(200, "Error1", "Error message");

        assertEquals(200, errorResponse.getStatus());
        assertEquals("Error1", errorResponse.getError());
        assertEquals("Error message", errorResponse.getMessage());

        errorResponse.setStatus(400);
        errorResponse.setError("BadError");
        errorResponse.setMessage("Bad message");

        assertEquals(400, errorResponse.getStatus());
        assertEquals("BadError", errorResponse.getError());
        assertEquals("Bad message", errorResponse.getMessage());
    }
}
