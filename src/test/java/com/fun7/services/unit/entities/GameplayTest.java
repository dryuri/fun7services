package com.fun7.services.unit.entities;

import com.fun7.services.entities.Gameplay;
import com.google.api.client.util.DateTime;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.TimeZone;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
public class GameplayTest {

    @Test
    public void testConstructorAndGetters() throws NoSuchFieldException, IllegalAccessException {
        DateTime now = new DateTime(new Date(), TimeZone.getTimeZone("UTC"));
        Gameplay gameplay = new Gameplay("Fun7user", now);

        Field privateId = Gameplay.class.getDeclaredField("id");
        privateId.setAccessible(true);

        privateId.set(gameplay, 123L);


        assertEquals("Fun7user", gameplay.getUserId());
        assertEquals(now, gameplay.getOpenedGame());
        assertEquals(123L, gameplay.getId());
    }

    @Test
    public void testSetters() {
        Gameplay gameplay = new Gameplay("", null);

        DateTime now = new DateTime(new Date(), TimeZone.getTimeZone("UTC"));

        gameplay.setOpenedGame(now);
        gameplay.setUserId("Fun7user");

        assertEquals("Fun7user", gameplay.getUserId());
        assertEquals(now, gameplay.getOpenedGame());
    }
}
