package com.fun7.services.integration;

import org.codehaus.jackson.annotate.JsonProperty;

public class ServicesResponseObject {
    private String ads;
    @JsonProperty("customer-support")
    private String customerSupport;
    private String multiplayer;

    public ServicesResponseObject(String ads, String customerSupport, String multiplayer) {
        this.ads = ads;
        this.customerSupport = customerSupport;
        this.multiplayer = multiplayer;
    }

    public String getAds() {
        return ads;
    }

    public void setAds(String ads) {
        this.ads = ads;
    }

    public String getCustomerSupport() {
        return customerSupport;
    }

    public void setCustomerSupport(String customerSupport) {
        this.customerSupport = customerSupport;
    }

    public String getMultiplayer() {
        return multiplayer;
    }

    public void setMultiplayer(String multiplayer) {
        this.multiplayer = multiplayer;
    }
}
