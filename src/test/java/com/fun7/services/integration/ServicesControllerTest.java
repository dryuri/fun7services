package com.fun7.services.integration;

import com.fun7.services.ServicesApplication;
import com.fun7.services.exceptions.ErrorResponse;
import com.fun7.services.repositories.GameRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.event.annotation.AfterTestClass;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(classes = ServicesApplication.class, webEnvironment = WebEnvironment.DEFINED_PORT)
public class ServicesControllerTest {

    @LocalServerPort
    private int port = 8080;

    private String api = "http://localhost:" + port + "/";

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private GameRepository gameRepository;

    @AfterTestClass
    public void tearDown() {
        gameRepository.deleteAll();
    }

    @Test
    public void test404() {
        assertEquals(404, restTemplate.getForObject(this.api, ErrorResponse.class).getStatus());
    }

    @Test
    public void testAllValidParametersDisabledMultiplayer() {
        String requestUrl = this.api + "services?userId=funuser&cc=US&timezone=Europe/Ljubljana";

        assertEquals("disabled", restTemplate.getForObject(requestUrl, ServicesResponseObject.class).getMultiplayer());
    }

    @Test
    public void testAllValidParametersEnabledMultiplayer() {
        String requestUrl = this.api + "services?userId=funuserPlay5&cc=US&timezone=Europe/Ljubljana";

        for (int i = 0; i < 5; i++) {
            restTemplate.getForObject(requestUrl, ServicesResponseObject.class);
        }

        assertEquals("enabled", restTemplate.getForObject(requestUrl, ServicesResponseObject.class).getMultiplayer());
    }

    @Test
    public void testResponseObjectIsCorrect() {
        String requestUrl = this.api + "services?userId=funuserCorrect&cc=US&timezone=Europe/Ljubljana";

        ServicesResponseObject response = restTemplate.getForObject(requestUrl, ServicesResponseObject.class);

        assertNotNull(response.getMultiplayer());
        assertNotNull(response.getAds());
        //TODO fix json customer-support serialization
        //assertNotNull(restTemplate.getForObject(requestUrl, ServicesResponseObject.class).getCustomerSupport());
    }

    @Test
    public void badRequestCcLengthTooShort() {
        String requestUrl = this.api + "services?userId=something&cc=U&timezone=Europe/Ljubljana";

        ErrorResponse response = restTemplate.getForObject(requestUrl, ErrorResponse.class);

        assertEquals(400, response.getStatus());
        assertEquals("services.cc: cc length must be 2", response.getMessage());
    }

    @Test
    public void badRequestUserIdLengthTooLong() {
        String requestUrl = this.api + "services?userId=thisisreallyloooooooooooooooooooooooooooongname&cc=US&timezone=Europe/Ljubljana";

        ErrorResponse response = restTemplate.getForObject(requestUrl, ErrorResponse.class);

        assertEquals(400, response.getStatus());
        assertEquals("services.userId: userId length must be between 1 and 30", response.getMessage());
    }

    @Test
    public void badRequestTimezoneLengthTooLong() {
        String requestUrl = this.api + "services?userId=normallength&cc=US&timezone=thisisreallyloooooooooooooooooooooooooooooooooooooooooooooooooongname";

        ErrorResponse response = restTemplate.getForObject(requestUrl, ErrorResponse.class);

        assertEquals(400, response.getStatus());
        assertEquals("services.timezone: timezone length must be between 1 and 50", response.getMessage());
    }

    @Test
    public void badRequestMissingUserIdParam() {
        String requestUrl = this.api + "services?cc=US&timezone=something";

        ErrorResponse response = restTemplate.getForObject(requestUrl, ErrorResponse.class);

        assertEquals(400, response.getStatus());
        assertEquals("Required String parameter 'userId' is not present", response.getMessage());
    }

    @Test
    public void badRequestMissingCcParam() {
        String requestUrl = this.api + "services?userId=normallength&timezone=something";

        ErrorResponse response = restTemplate.getForObject(requestUrl, ErrorResponse.class);

        assertEquals(400, response.getStatus());
        assertEquals("Required String parameter 'cc' is not present", response.getMessage());
    }

    @Test
    public void badRequestMissingTimezoneParam() {
        String requestUrl = this.api + "services?userId=normallength&cc=US";

        ErrorResponse response = restTemplate.getForObject(requestUrl, ErrorResponse.class);

        assertEquals(400, response.getStatus());
        assertEquals("Required String parameter 'timezone' is not present", response.getMessage());
    }
}
