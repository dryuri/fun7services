package com.fun7.services.services;

import com.fun7.services.controllers.UserData;

import java.util.Set;

public interface Services {

    Set<String> getSystemServices();

    boolean isServiceEnabled(String service, UserData userData);
}
