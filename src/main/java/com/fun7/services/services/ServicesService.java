package com.fun7.services.services;

import com.fun7.services.controllers.UserData;
import com.fun7.services.services.system.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.TreeSet;

/**
 * ServicesService provides us with
 */
@Service
public class ServicesService implements Services {

    final private AdsService adsService;

    final private CustomerSupportService customerSupportService;

    final private MultiplayerService multiplayerService;

    @Autowired
    public ServicesService(AdsService adsService, CustomerSupportService customerSupportService, MultiplayerService multiplayerService) {
        this.adsService = adsService;
        this.customerSupportService = customerSupportService;
        this.multiplayerService = multiplayerService;
    }

    /**
     * getSystemServices returns a Set of all available services in our system. In a more complex system this would
     * most likely be saved in a database.
     * <p>
     * All services are lowercase
     *
     * @return
     */
    public Set<String> getSystemServices() {
        Set<String> enabledServices = new TreeSet<>();
        enabledServices.add("multiplayer");
        enabledServices.add("customer-support");
        enabledServices.add("ads");

        return enabledServices;
    }

    @Override
    public boolean isServiceEnabled(String service, UserData userData) {
        switch (service) {
            case "multiplayer":
                return multiplayerService.isEnabled(userData);
            case "customer-support":
                return customerSupportService.isEnabled(userData);
            case "ads":
                return adsService.isEnabled(userData);
            // We can very easily add support for new services here!
            default:
                return false;
        }
    }
}
