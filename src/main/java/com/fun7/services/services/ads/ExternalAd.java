package com.fun7.services.services.ads;

import com.fun7.services.controllers.UserData;

public interface ExternalAd {
    ExternalAdResponse getAds(UserData userData);
}
