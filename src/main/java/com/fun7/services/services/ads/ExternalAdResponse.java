package com.fun7.services.services.ads;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalAdResponse {
    private String ads;

    public ExternalAdResponse() {

    }

    public ExternalAdResponse(String ads) {
        this.ads = ads;
    }

    public String getAds() {
        return ads;
    }

    public void setAds(String ads) {
        this.ads = ads;
    }
}
