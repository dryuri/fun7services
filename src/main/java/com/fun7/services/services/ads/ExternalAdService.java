package com.fun7.services.services.ads;

import com.fun7.services.controllers.UserData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Base64;

@Service
public class ExternalAdService implements ExternalAd {

    private final RestTemplate restTemplate;

    @Autowired
    public ExternalAdService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Cacheable(value = "externalAdsCache", key = "#userData.deviceUserAgent")
    @Override
    public ExternalAdResponse getAds(UserData userData) {
        String adExternalUIrl = "https://us-central1-o7tools.cloudfunctions.net/fun7-ad-partner";
        String user = "fun7user";
        String pass = "fun7pass";

        String authHeader = "Basic " + Base64.getEncoder().encodeToString((user + ":" + pass).getBytes());

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Authorization", authHeader);
        httpHeaders.set("User-Agent", userData.getDeviceUserAgent());

        HttpEntity httpEntity = new HttpEntity(httpHeaders);

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(adExternalUIrl)
                .queryParam("countryCode", userData.getCc());

        try {
            ResponseEntity<ExternalAdResponse> response = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.GET, httpEntity, ExternalAdResponse.class);
            return response.getBody();
        } catch (Exception e) {
            throw e; // Rethrow to not cache anything!
        }
    }
}
