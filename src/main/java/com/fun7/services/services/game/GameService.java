package com.fun7.services.services.game;

import com.fun7.services.entities.Gameplay;
import com.fun7.services.repositories.GameRepository;
import com.google.api.client.util.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.TimeZone;

@Service
public class GameService implements Game {

    private final GameRepository gameRepository;

    @Autowired
    public GameService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    @Override
    public void playGame(String userId) {
        try {
            gameRepository.save(new Gameplay(userId, new DateTime(new Date(), TimeZone.getTimeZone("UTC"))));
        } catch (Exception e) {
            // TODO what happens if this fails?
        }
    }

    @Override
    public boolean isCountryAllowed(String cc) {
        Set<String> allowedCountries = new HashSet<>();
        allowedCountries.add("US");

        return allowedCountries.contains(cc.toUpperCase());
    }
}
