package com.fun7.services.services.game;

public interface Game {
    void playGame(String userId);
    boolean isCountryAllowed(String cc);
}
