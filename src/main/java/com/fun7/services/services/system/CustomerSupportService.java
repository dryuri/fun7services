package com.fun7.services.services.system;

import com.fun7.services.controllers.UserData;
import com.fun7.services.utils.HolidayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.TimeZone;

@Service
public class CustomerSupportService implements Fun7Service {

    private final HolidayService holidayService;

    @Autowired
    public CustomerSupportService(HolidayService holidayService) {
        this.holidayService = holidayService;
    }

    @Override
    public boolean isEnabled(UserData userData) {
        // Configuration for Ljubljana local time working hours of customer support team
        int SUPPORT_START_HOUR = 9;
        int SUPPORT_END_HOUR = 15;

        ZonedDateTime localLjubljanaTime = holidayService.getLjubljanaTimeNow();

        if (!holidayService.isWorkDay(localLjubljanaTime)) {
            return false;
        }

        return localLjubljanaTime.getHour() > SUPPORT_START_HOUR
                && localLjubljanaTime.getHour() < SUPPORT_END_HOUR;
    }
}
