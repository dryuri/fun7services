package com.fun7.services.services.system;

import com.fun7.services.controllers.UserData;
import com.fun7.services.repositories.GameRepository;
import com.fun7.services.services.game.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class MultiplayerService implements Fun7Service {

    private final GameService gameService;

    private final GameRepository gameRepository;

    @Autowired
    public MultiplayerService(GameService gameService, GameRepository gameRepository) {
        this.gameService = gameService;
        this.gameRepository = gameRepository;
    }

    @Override
    public boolean isEnabled(UserData userData) {
        int TRESHOLD_FOR_MULTIPLAYER = 5;
        return (gameRepository.countByUserId(userData.getUserId()) >= TRESHOLD_FOR_MULTIPLAYER)
                && gameService.isCountryAllowed(userData.getCc());
    }
}
