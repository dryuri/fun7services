package com.fun7.services.services.system;

import com.fun7.services.controllers.UserData;
import com.fun7.services.services.ads.ExternalAdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdsService implements Fun7Service {

    private ExternalAdService externalAdService;

    @Autowired
    public AdsService(ExternalAdService externalAdService) {
        this.externalAdService = externalAdService;
    }

    @Override
    public boolean isEnabled(UserData userData) {
        try {
            return externalAdService.getAds(userData).getAds().equals("sure, why not!");
        } catch (Exception e) {
            return false;
        }
    }
}
