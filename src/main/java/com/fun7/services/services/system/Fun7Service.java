package com.fun7.services.services.system;

import com.fun7.services.controllers.UserData;

public interface Fun7Service {
    boolean isEnabled(UserData userData);
}
