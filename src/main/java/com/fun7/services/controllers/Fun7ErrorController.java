package com.fun7.services.controllers;

import com.fun7.services.exceptions.Fun7NotFoundException;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Fun7ErrorController implements ErrorController {

    @RequestMapping("/error")
    public String handleError() {
        throw new Fun7NotFoundException("Resource not found");
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
