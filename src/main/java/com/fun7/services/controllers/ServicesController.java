package com.fun7.services.controllers;

import com.fun7.services.services.ServicesService;
import com.fun7.services.services.game.GameService;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;
import java.util.HashMap;
import java.util.Map;

@RestController
@Validated
public class ServicesController {

    final private ServicesService servicesService;

    final private GameService gameService;

    @Autowired
    public ServicesController(ServicesService servicesService, GameService gameService) {
        this.servicesService = servicesService;
        this.gameService = gameService;
    }

    @GetMapping("/services")
    public Map<String, String> services(
            @RequestHeader(value = "User-Agent", required = false) String userAgent,
            @RequestParam(required = true, name = "userId") @NotBlank @Length(min = 1, max = 30, message = "userId length must be between 1 and 30") String userId,
            @RequestParam(required = true, name = "cc") @NotBlank @Length(min = 2, max = 2, message = "cc length must be 2") String cc,
            @RequestParam(required = true, name = "timezone") @NotBlank() @Length(min = 1, max = 50, message = "timezone length must be between 1 and 50") String timezone) {

        // Mark user playing the game
        this.gameService.playGame(userId);

        Map<String, String> services = new HashMap<>();
        for (String service : servicesService.getSystemServices()) {
            services.put(service, servicesService.isServiceEnabled(service, new UserData(userId, cc, timezone, userAgent)) ? "enabled" : "disabled");
        }

        return services;
    }
}
