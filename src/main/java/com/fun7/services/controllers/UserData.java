package com.fun7.services.controllers;

public class UserData {
    private String userId;
    private String cc;
    private String timezone;
    private String deviceUserAgent;

    public UserData(String userId, String cc, String timezone, String deviceUserAgent) {
        this.userId = userId;
        this.cc = cc;
        this.timezone = timezone;
        this.deviceUserAgent = deviceUserAgent;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getDeviceUserAgent() {
        return deviceUserAgent;
    }

    public void setDeviceUserAgent(String deviceUserAgent) {
        this.deviceUserAgent = deviceUserAgent;
    }
}
