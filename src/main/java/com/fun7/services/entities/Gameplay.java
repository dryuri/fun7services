package com.fun7.services.entities;

import com.google.api.client.util.DateTime;
import org.springframework.cloud.gcp.data.datastore.core.mapping.Entity;
import org.springframework.data.annotation.Id;

@Entity(name = "game")
public class Gameplay {

    @Id
    private Long id;

    private String userId;

    private DateTime openedGame;

    public Gameplay(String userId, DateTime openedGame) {
        this.userId = userId;
        this.openedGame = openedGame;
    }

    public Long getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public DateTime getOpenedGame() {
        return openedGame;
    }

    public void setOpenedGame(DateTime openedGame) {
        this.openedGame = openedGame;
    }
}
