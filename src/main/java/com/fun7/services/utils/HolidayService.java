package com.fun7.services.utils;

import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import static com.google.common.primitives.Ints.contains;

@Service
public class HolidayService {
    public boolean isWorkDay(ZonedDateTime localLjubljanaTime) {
        // Disabled on Saturday and Sunday
        int[] weekend = {6, 7};
        if (contains(weekend, localLjubljanaTime.getDayOfWeek().getValue())) {
            return false;
        }

        // Also disabled on holidays
        List<Holiday> holidays = this.getHolidays();
        for (Holiday holiday : holidays) {
            if (holiday.getMonth() == localLjubljanaTime.getMonthValue()
                    && holiday.getDay() == localLjubljanaTime.getDayOfMonth()) {
                return false;
            }
        }
        return true;
    }

    public List<Holiday> getHolidays() {
        List<Holiday> holidays = new ArrayList<>();

        // Christmas example
        holidays.add(new Holiday(25, 12));
        holidays.add(new Holiday(26, 12));
        // This can further be developed by storing holidays in DB or serving from another service.
        return holidays;
    }

    public ZonedDateTime getLjubljanaTimeNow() {
        TimeZone timeZoneLjubljana = TimeZone.getTimeZone("Europe/Ljubljana");
        return ZonedDateTime.now(timeZoneLjubljana.toZoneId());
    }
}
