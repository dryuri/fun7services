package com.fun7.services.exceptions;

public class Fun7NotFoundException extends RuntimeException {
    public Fun7NotFoundException(String message) {
        super(message);
    }
}
