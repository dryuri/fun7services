package com.fun7.services.repositories;

import com.fun7.services.entities.Gameplay;
import org.springframework.cloud.gcp.data.datastore.repository.DatastoreRepository;

public interface GameRepository extends DatastoreRepository<Gameplay, Long> {
    int countByUserId(String userId);
}
