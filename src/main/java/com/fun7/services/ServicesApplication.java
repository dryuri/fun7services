package com.fun7.services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@SpringBootApplication
@EnableCaching
@ComponentScan(basePackages = {
        "com.fun7.services.controllers",
        "com.fun7.services.services",
        "com.fun7.services.entities",
        "com.fun7.services.repositories",
        "com.fun7.services.utils",
        "com.fun7.services.exceptions"
})
public class ServicesApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServicesApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
        long timeoutForExternalApis = 3L;
        return restTemplateBuilder.setReadTimeout(Duration.ofSeconds(timeoutForExternalApis)).setConnectTimeout(Duration.ofSeconds(timeoutForExternalApis)).build();
    }
}
