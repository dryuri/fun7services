# Fun7 Services

Instructions for this project can be found in [google document](https://docs.google.com/document/d/17rGvlAuHGNBcYlDeIrM8dB-M_aoSWKj8wZjb-Kj1DTU/edit).

# Assumptions

* If one of the services throws an exception or has an error we can return it as disabled. This allows application
to get statuses of other services without interruptions.
* Assumption was made that query parameter 'timezone' is in the assignment as a trick question since
it makes no difference what timezone mobile application sends. Ljubljana time is always the same and can be 
checked without this parameter.
* For customer-support service work day is defined as Monday to Friday with possible holiday during the week.
Holiday dates have been hardcoded for simplicity.
* External ads api returns enabled ads if user device is supported. Assumption was made that user device
is User-Agent header client sends to our api. This was used for caching the external api response.

# Prerequisites

* Maven (https://maven.apache.org/)
* Java (min version 8)
* Google Cloud SDK (https://cloud.google.com/sdk/install)

# TL;DR

```
gcloud projects create MY_PROJECT_ID --set-as-default
```

```
gcloud app create --region europe-west3
```

```
mvn -DskipTests -Dgcp.projectId=MY_PROJECT_ID package appengine:deploy
```

```
gcloud app browse
```

```
MY_APP_URL/services?userId=fun7user&cc=US&timezone=Europe/Ljubljana
```

# Dev environment

# Run locally

Since we are using Datastore we must configure GCP credentials by setting environment variable pointing to
credentials json.

```
GOOGLE_APPLICATION_CREDENTIALS
```

Check [GCP docs](https://cloud.google.com/docs/authentication/production#obtaining_and_providing_service_account_credentials_manually) for details and how to generate credentials json.

Test run the app.
```
mvn -DskipTests spring-boot:run
```
Open the app in web browser to verify it is working.
```
localhost:8080/services
```

## Run tests

### Set up local Datastore server

We do not want our tests to hit production database so we need to run GCP's local Datastore server.

Install cloud datastore emulator

```
gcloud components install cloud-datastore-emulator
```

And run it

```
gcloud beta emulators datastore start --no-store-on-disk
```

We need to set up ENV variables to tell our application to use local instance and not connect to the cloud.

```
DATASTORE_EMULATOR_HOST=localhost:PORT
DATASTORE_PROJECT_ID=my-project-id
```

(Port will be shown in the console where you start your emulator, and project id can be
 seen again by running gcloud projects list)
 
### Run tests
 
Run test command
 
```
mvn clean test
```
 
### Check report
 
When tests are run test coverage report is also generated. Check html page for details

```
target/site/jacoco/index.html
```   

# Deploy

### Configure

Before deploying application to the Google Cloud App Engine you can set desired instance class.

[Check prices](https://cloud.google.com/appengine/docs/standard/) for instance classes

Update config with desired instance class

```
/src/main/appengine/app.yaml
```

### Create project

Create project in console
```
gcloud projects create MY_PROJECT_ID --set-as-default
```

To see all your projects you can see them in console

```
gcloud projects list
```

And then select the one you created before. 

(You must do this if you created project in web interface)

```
gcloud config set project MY_PROJECT ID
```

### Create app

Create app in desired region. Select something close to you / your target users for REGION.

Use Frankfurt region for example as "europe-west3" 

```
gcloud app create --region REGION
```

### Set Datastore to Native mode (optional)

In GCP Console, navigate to Menu -> Datastore (in the Storage section) and select Native mode if you want. 

### Deploy to App engine

Deploy to the GCP (It might take a few minutes. Do not worry if it looks like it got stuck)

```
mvn -DskipTests -Dgcp.projectId=MY_PROJECT_ID package appengine:deploy
```

### Open

After your deploy is completed you can run a simple command to open you app in browser

```
gcloud app browse
```

## Possible improvements

* Remove hardcoded fun7 user and password from ExternalAdService and put them into something else. Secrets manager, env variables...
* Move hardcoded available services and countries for multiplay into another service or database
* Introduce caching for fetching if user has played at least 5 times and has multiplayer service enabled
* Setup continuous integration pipeline to automate building, testing and deploying application.